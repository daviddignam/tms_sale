# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging

from openerp import SUPERUSER_ID
from openerp.osv import fields, osv

_logger = logging.getLogger(__name__)


class uploadtool_configuration(osv.TransientModel):
	_inherit = 'sale.config.settings'
	
	_columns = {
		'default_so_tax': fields.many2one('account.tax', "Default Tax",
			help='Default Tax item used when automatically creating a quotation from the uploadtool.'),
	}
	
	def set_tax_defaults(self, cr, uid, ids, context=None):
		try:
			default_tax = self.browse(cr, uid, ids, context=context).default_so_tax
			res = self.pool.get('ir.values').set_default(cr, SUPERUSER_ID, 'sale.config.settings', 'default_so_tax', default_tax.id)
			return res
		except Exception:
			_logger.error("Error setting so tax", exc_info=True)